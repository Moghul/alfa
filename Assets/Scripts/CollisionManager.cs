﻿using UnityEngine;
using System.Collections;

public abstract class CollisionManager : MonoBehaviour {

	//since the player needs to be kinematic in order to behave properly
	//it means that it can not normally collide with walls
	//this means that I had to handle walking into them
	//if you run into a top wall, your position on the z axis is lowered by 0.1
	//the same applies to the other walls.

	public virtual void OnTriggerStay(Collider other)
	{
		doWallChecks(other);
	}

	void OnTriggerEnter(Collider other) 
	{
		doWallChecks(other);
	}

	public virtual void doWallChecks(Collider other)
	{
		if (other.tag=="TopWall")
		{
			ClickToMove.targetPosition=new Vector3(transform.position.x, transform.position.y, transform.position.z-0.1f);
		}else 
			if(other.tag=="BottomWall")
		{
			ClickToMove.targetPosition=new Vector3(transform.position.x, transform.position.y, transform.position.z+0.1f);
		}else 
			if(other.tag=="LeftWall")
		{
			ClickToMove.targetPosition=new Vector3(transform.position.x+0.1f, transform.position.y, transform.position.z);
		}else 
			if(other.tag=="RightWall")
		{
			ClickToMove.targetPosition=new Vector3(transform.position.x-0.1f, transform.position.y, transform.position.z);
		}
	}
	

}
