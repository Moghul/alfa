﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkManagerScript : MonoBehaviour 
{
	private string gameName = "Silviu_Ion_ALFA", serverName;
	private bool refreshing;
	public HostData[] hostData;

	public static NetworkManagerScript instance;
	public static bool isClient;

	void Awake () 
	{
		instance = this;
	}

	public void startServer(string server)
	{
		Network.InitializeServer (5, 25000, !Network.HavePublicAddress());
		MasterServer.RegisterHost (gameName, server, "This is an exam project game");
	}

	public void refreshHostList()
	{
		MasterServer.RequestHostList (gameName);
		refreshing = true;
	}

	void Update()
	{
		if (Network.isClient) 
		{
			isClient = true;
		} else 
		{
			isClient = false;
		}

		if (refreshing) 
		{
			if(MasterServer.PollHostList ().Length >0)
			{
				refreshing = false;
				Debug.Log (MasterServer.PollHostList ().Length);
				hostData = MasterServer.PollHostList();
			}
		}
	}
	
	void OnServerInitialized ()
	{
		Debug.Log ("Server Initialized");
	}

	void OnMasterServerEvent(MasterServerEvent mse)
	{
		if (mse == MasterServerEvent.RegistrationSucceeded) 
		{
			Debug.Log ("Registered server");
		}
	}
}
