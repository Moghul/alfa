﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class LevelRandomizer : MonoBehaviour {

	public Transform[] possibleRooms;
	public List<Transform> actualRooms;
	public static LevelRandomizer instance;
	public Transform level;
	public Transform t;

	public string savePath, nextLevelPath;
	public List<string> saveData = new List<string> ();


	//this is where I decide where the save files will be
	//they are located in C:\Users\[USER NAME]\AppData\LocalLow\DefaultCompany\UnityProjects\SaveGame
	//they are two kinds: one where I save the game for a different game session
	//and one where I save the player's state for the next level
	//You will notice that I am not saying what kind of file it is (.txt or .dat). The reason behind this is that
	//the game can still read it but the player will have to jump through a few hoops to open and alter the save file
	void Awake()
	{
		savePath = Application.persistentDataPath + "/SaveGame/level";
		nextLevelPath = Application.persistentDataPath + "/SaveGame/nextlevel";
		instance = this;

		if (!Directory.Exists(Application.persistentDataPath + "/SaveGame"))
		{
			Directory.CreateDirectory(Application.persistentDataPath + "/SaveGame");
		}

		StartCoroutine(CreateLevel ());
	}

	public void destroyChildren()
	{
		int childs = transform.childCount;
		
		for (int i = childs - 1; i > 0; i--)
		{
			if(Network.isServer)
			{
				Network.Destroy(transform.GetChild(i).gameObject);
				GameObject.Destroy(transform.GetChild(i).gameObject);
			}else if(!Network.isClient)
			{
				GameObject.Destroy(transform.GetChild(i).gameObject);
			}
		}
	}


	//we want to delete the next level path because if the player exits the game without saving, he shouldn't
	//be able to continue where he left off
	void OnApplicationQuit()
	{
		if (File.Exists(nextLevelPath))
		{
			File.Delete(nextLevelPath);
		}
	}


	/// <summary>
	/// First we check if we are moving to the next level
	/// we create a new one
	/// we give the player the stats it was left with
	/// otherwise, if the player saved the game, it will automatically load the level he was last on.
	/// If there are no save files it means the is starting over and we randomize a whole new level
	/// </summary>
	IEnumerator CreateLevel()
	{

		//this is the simplest way to make sure that the connection is made before loading the level. 
		//I realize it isn't perfect but it is functional
		//NetworkManagerScript.isClient doesn't immediately return true when it should

		yield return new WaitForSeconds (1.0f);

		if (NetworkManagerScript.isClient == false)
		{

			if (PlayerPrefs.GetInt("save")==1){
				if (File.Exists (nextLevelPath)) 
				{
					RandomizeLevel ();
					LevelLoader.instance.ParseFromSaveFile (nextLevelPath);
					PlayerScript.instance.health = float.Parse(saveData [0]);
					Debug.Log (saveData[0]);
				} else 
				{
					RandomizeLevel();
				}
			}
			else {
				if (File.Exists (Application.persistentDataPath + "/SaveGame/level")) 
				{
					LevelLoader.instance.ParseFromSaveFile (Application.persistentDataPath + "/SaveGame/level");
					LevelLoader.instance.LoadLevel();
				} else 
				{
					RandomizeLevel();
				}
			}
		}

		PlayerPrefs.SetInt ("save", 1);
	}

	/// <summary>
	/// Simple randomizer, it sets the start and end pad and picks the other 7 pads randomly.
	/// Choosing a 3x3 grid is arbitrary, I just felt that a level shouldn't take too long
	/// to avoid boredom. Also, if there are too many rooms, the player has a bigger chance of dying
	/// and the game should be pretty difficult already
	/// </summary>
	public void RandomizeLevel()
	{
		for(int i = 0; i<3; i++)
		{
			for(int j = 0; j<3; j++)
			{
				Vector3 pos = new Vector3(i*20f, -3f, j*20f);
				Quaternion rot = new Quaternion(0f, 0f, 0f, 0f);
				if(i==j && i==2)
				{
					if (Network.isServer)
					{
						t=Network.Instantiate(possibleRooms[0], pos, rot, 0) as Transform;
						//t.transform.parent = level;
						NetworkParentSolver.instance.solveParenting(t, level);
					}else if (!Network.isClient)
					{
						t=Instantiate(possibleRooms[0], pos, rot) as Transform;
						t.transform.parent = level;
					}
				}else
					if(i==j && j==0)
				{
					if (Network.isServer)
					{
						t=Network.Instantiate(possibleRooms[1], pos, rot, 0) as Transform;
						//t.transform.parent = level;
						NetworkParentSolver.instance.solveParenting(t, level);
					}else if (!Network.isClient)
					{
						t=Instantiate(possibleRooms[1], pos, rot) as Transform;
						t.transform.parent = level;
					}
				}else
				{
					int x = Random.Range(2, possibleRooms.Length);
					if (Network.isServer)
					{
						t=Network.Instantiate(possibleRooms[x], pos, rot, 0) as Transform;
						//t.transform.parent = level;
						NetworkParentSolver.instance.solveParenting(t, level);
					}else if (!Network.isClient)
					{
						t=Instantiate(possibleRooms[x], pos, rot) as Transform;
						t.transform.parent = level;
					}
				}
				actualRooms.Add(t);
			}
		}
	}
}
