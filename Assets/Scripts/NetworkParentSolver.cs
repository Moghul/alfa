﻿using UnityEngine;
using System.Collections;

public class NetworkParentSolver : MonoBehaviour 
{
	public static NetworkParentSolver instance;

	void Start ()
	{
		instance = this;
	}

	public void solveParenting (Transform child, Transform parent) {
		if(networkView.isMine)
		{
			child.transform.parent = parent;
			networkView.RPC("addToParent", RPCMode.OthersBuffered, child.networkView.viewID);
		}
	}
	
	
	[RPC] public void addToParent(NetworkViewID id) {
		
		Transform t = NetworkView.Find(id).transform;
		t.parent = transform;
	}



	public string GetMountPath()
	{
		string str = "";
		Transform cur = transform.parent;
		while( cur.parent != null )
		{
			str = cur.name + "/" + str;
			cur = cur.parent;
		}
		return str.Trim( "/".ToCharArray() );
	}
	
	[RPC]
	public void RPCLinkToParent( string mountPath, NetworkViewID rootID, Vector3 lPos, Vector3 lEuler )
	{
		Transform rootPoint = NetworkView.Find( rootID ).transform;
		//Debug.Log( "Got SyncMount for " + mountPath + " on object " + rootPoint.gameObject.name );
		StartCoroutine( DoMount( mountPath, rootPoint, lPos, lEuler ) );
	}
	
	IEnumerator DoMount( string mountPath, Transform rootPoint, Vector3 lPos, Vector3 lEuler )
	{
		Transform mountPoint = rootPoint.transform.Find( mountPath );
		while( mountPoint == null )
		{
			//Debug.Log( "Mount point didn't exist yet" );
			yield return new WaitForSeconds( 0.5f );
			mountPoint = rootPoint.transform.Find( mountPath );
		}
		
		//Debug.Log( "Mounting object " + gameObject.name + " to position " + lPos );
		transform.parent = mountPoint;
		transform.localEulerAngles = lEuler;
		transform.localPosition = lPos;
	}
	
	public void SyncMount()
	{
		if( networkView.isMine )
		{
			//Debug.Log( "Sending SyncMount for " + gameObject.name, gameObject );
			string mountPath = GetMountPath();
			NetworkViewID vID = transform.root.networkView.viewID;
			//Debug.Log( "Root path is " + mountPath + ", root ID is " + vID );
			networkView.RPC( "RPCLinkToParent", RPCMode.OthersBuffered, mountPath, vID, transform.localPosition, transform.localEulerAngles );
		}
	}
}
