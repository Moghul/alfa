﻿using UnityEngine;
using System.Collections;

public class PlayerCollisionManager : CollisionManager 
{
	public Transform gui;


	public virtual void OnTriggerStay(Collider other)
	{
		doWallChecks(other);
	}
	
	void OnTriggerEnter(Collider other) 
	{
		doWallChecks(other);

		if (other.tag=="menu")
		{
			gui.gameObject.SetActive(true);
		}




		//if you touch the stairs, they take you to the next level
		//by saving your current state
		//and reloading the scene
		if(other.tag=="NextLevel")
		{
			LevelLoader.instance.PrepareNextLevel();
			ReloadGameLevel(); 
			if(Network.isServer)
			{
				networkView.RPC("ReloadGameLevel", RPCMode.Server);
			}
			ClickToMove.targetPosition = new Vector3(0, transform.position.y, 0);
		}

		//touching the portal will take you to the boss fight
		if(other.tag=="BossFight")
		{
			LoadBossLevel();
			if(Network.isServer)
			{
				networkView.RPC("LoadBossLevel", RPCMode.Server);
			}
			ClickToMove.targetPosition = new Vector3(0, transform.position.y, 20);
		}
	}


	void OnTriggerExit()
	{
		if(Application.loadedLevel==1)
		{
			gui.gameObject.SetActive (false);
		}
	}

	[RPC]
	void ReloadGameLevel()
	{
		Application.LoadLevel (1);
	}

	[RPC]
	void LoadBossLevel()
	{
		Application.LoadLevel (2);
	}
}
