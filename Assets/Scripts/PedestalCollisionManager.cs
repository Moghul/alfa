﻿using UnityEngine;
using System.Collections;

public class PedestalCollisionManager : MonoBehaviour {

	public Transform stairs;
	public Transform portal;

	void OnTriggerEnter(Collider other) 
	{
		if(other.tag == "Player")
		{
			if(Network.isServer)
			{
				stairs.gameObject.SetActive (true);
				portal.gameObject.SetActive (true);
				networkView.RPC("EnableObject", RPCMode.OthersBuffered, true);
			}else if(!Network.isClient)
			{
				stairs.gameObject.SetActive(true);
				portal.gameObject.SetActive(true);
			}
		}
	}

	[RPC]
	public void EnableObject(bool x) 
	{
		stairs.gameObject.SetActive (x);
		portal.gameObject.SetActive (x);
	}
}
