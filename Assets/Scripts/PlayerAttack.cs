﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAttack : MonoBehaviour {

	public Transform projectile, aura, skillbar, cleave, consecration;
	public static string specificAttack;
	public List <Transform> tempAura;
	public GUITexture qAttack, wAttack, eAttack, rAttack;
	public Texture qAttackA, wAttackA, eAttackA, rAttackA, qAttackB, wAttackB, eAttackB, rAttackB;
	public GUIText q, w, e, r;
	public static Vector3 targetPosition, targetPoint;


	private float TeleportCooldown, AuraCooldown, AuraDuration, CleaveCooldown, ConsecrationCooldown;
	private bool HasAura;
	private Quaternion TargetRotation;
	private Ray Ray;
	private Plane PlayerPlane;
	private float Hitdist;

	// Use this for initialization
	void Start () 
	{
		AuraDuration = 10f;
		specificAttack="d";
	}
	
	// Update is called once per frame
	void Update () 
	{
		DoSkillbarChanges ();

		if(Application.loadedLevel !=0 && !Network.isClient)
		{
			skillbar.gameObject.SetActive(true);
		}
		else
		{
			skillbar.gameObject.SetActive(false);
		}

		if (!Network.isClient)
		{
			TeleportCooldown -= Time.deltaTime;
			AuraCooldown -= Time.deltaTime;
			CleaveCooldown -= Time.deltaTime;
			ConsecrationCooldown -= Time.deltaTime;

			if (HasAura) 
			{
				if (AuraDuration <= 0) 
				{
					if(Network.isServer)
					{
						aura.gameObject.SetActive (false);
						networkView.RPC("ToggleAura", RPCMode.OthersBuffered, false);
					}
					else if(!Network.isClient)
					{
						aura.gameObject.SetActive (false);
					}
					AuraDuration = 10f;
				} else 
				{
					AuraDuration -= Time.deltaTime;
				}
			}
		}

		DoButtonChecks ();

	}

	//since the trigger for each attack is left mouse button
	//I need to handle modifiers
	//doDefaultAttack is mentioned twice because otherwise there is no way to switch from teleports to projectiles.
	//By setting the string to "d" every time I teleport or do anything else,
	//i automatically switch back to the regular attack.
	private void performAttack(string x)
	{
		switch(x)
		{
			case "d" : doDefaultAttack(); break;
			case "GUI" : GUIHasBeenPressed(); break;
			case "q" : doQAttack(); break;
			case "w" : doWAttack(); break;
			case "e" : doEAttack (); break;
			case "r" : doRAttack(); break;
			default : doDefaultAttack(); break;
		}
	}

	void DoButtonChecks()
	{
		if(Input.GetKeyDown (KeyCode.Mouse0))
		{
			performAttack (specificAttack);
		}
		
		if (Input.GetKeyDown (KeyCode.Q))
		{
			qAttack.texture = qAttackB;
			specificAttack = "q";
		}
		
		if (Input.GetKeyDown (KeyCode.W))
		{
			wAttack.texture = wAttackB;
			specificAttack = "w";
		}
		
		if (Input.GetKeyDown (KeyCode.E))
		{
			eAttack.texture = eAttackB;
			specificAttack = "e";
		}
		
		if (Input.GetKeyDown (KeyCode.R))
		{
			rAttack.texture = rAttackB;
			specificAttack = "r";
		}
	}

	/// <summary>
	/// this method exists because when you press a button in the gui, I don't
	/// want the character to move or to attack
	/// </summary>
	void GUIHasBeenPressed()
	{
		specificAttack = "d";
	}

	//spawns a projectile
	void doDefaultAttack()
	{
		Ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		PlayerPlane = new Plane (Vector3.up, transform.position);

		if (PlayerPlane.Raycast (Ray, out Hitdist)) 
		{
			targetPoint = Ray.GetPoint (Hitdist);
			targetPosition = Ray.GetPoint (Hitdist);
			TargetRotation = Quaternion.LookRotation (targetPoint - transform.position);
			transform.rotation = TargetRotation;
		}

		if (Network.isServer) 
		{
			Transform t = Network.Instantiate (projectile, this.transform.position, this.transform.rotation, 0) as Transform;
			ProjectileScript traj = t.gameObject.GetComponent<ProjectileScript> ();
			traj.targetPosition = targetPosition;
			traj.targetRotation = TargetRotation;
			traj.startingPosition = this.transform.position;
		}
		else if (!Network.isClient)
		{
			Transform t = Instantiate (projectile, this.transform.position, this.transform.rotation) as Transform;
			ProjectileScript traj = t.gameObject.GetComponent<ProjectileScript> ();
			traj.targetPosition = targetPosition;
			traj.targetRotation = TargetRotation;
			traj.startingPosition = this.transform.position;
		}
	}


	//teleports the player
	void doQAttack()
	{
		specificAttack = "d";

		Ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		PlayerPlane = new Plane (Vector3.up, transform.position);
		
		if (PlayerPlane.Raycast (Ray, out Hitdist)) 
		{
			targetPoint = Ray.GetPoint (Hitdist);
			targetPosition = Ray.GetPoint (Hitdist);
			TargetRotation = Quaternion.LookRotation (targetPoint - transform.position);
			transform.rotation = TargetRotation;
		}

		if(TeleportCooldown <=0)
		{
			this.transform.position = targetPosition;
			ClickToMove.targetPosition = this.transform.position;
			TeleportCooldown = 20;
		}
	}

	[RPC]
	public void ToggleAura(bool x) 
	{
		aura.gameObject.SetActive (x);
	}

	//spawns an aura
	void doWAttack()
	{
		if (AuraCooldown<=0)
		{
			Vector3 auraPosition = new Vector3 (this.transform.position.x, this.transform.position.y-0.1f ,this.transform.position.z);

			if(Network.isServer)
			{
				aura.gameObject.SetActive(true);
				networkView.RPC("ToggleAura", RPCMode.OthersBuffered, true);
			}else if(!Network.isClient)
			{					
				aura.gameObject.SetActive(true);
			}
			HasAura=true;

			aura.transform.position = auraPosition;

			AuraCooldown = 25f;
		}
		specificAttack = "d";
	}


	//todo
	void doEAttack()
	{
		specificAttack = "d";
		if (CleaveCooldown<=0)
		{
			Ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			PlayerPlane = new Plane (Vector3.up, transform.position);
			
			if (PlayerPlane.Raycast (Ray, out Hitdist)) 
			{
				targetPoint = Ray.GetPoint (Hitdist);
				targetPosition = Ray.GetPoint (Hitdist);
				TargetRotation = Quaternion.LookRotation (targetPoint - transform.position);
				transform.rotation = TargetRotation;
			}
			
			if (Network.isServer) 
			{
				Transform t = Network.Instantiate (cleave, this.transform.position, this.transform.rotation, 0) as Transform;
				CleaveBehaviour traj = t.gameObject.GetComponent<CleaveBehaviour> ();
				traj.targetPosition = targetPosition;
				traj.targetRotation = TargetRotation;
				traj.startingPosition = this.transform.position;
			}
			else if (!Network.isClient)
			{
				Transform t = Instantiate (cleave, this.transform.position, this.transform.rotation) as Transform;
				CleaveBehaviour traj = t.gameObject.GetComponent<CleaveBehaviour> ();
				traj.targetPosition = targetPosition;
				traj.targetRotation = TargetRotation;
				traj.startingPosition = this.transform.position;
			}
			CleaveCooldown=10;
		}
	}
	//todo
	void doRAttack()
	{
		specificAttack = "d";
		if (ConsecrationCooldown<=0)
		{
			Ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			PlayerPlane = new Plane (Vector3.up, transform.position);
			
			if (PlayerPlane.Raycast (Ray, out Hitdist)) 
			{
				targetPoint = Ray.GetPoint (Hitdist);
				targetPosition = Ray.GetPoint (Hitdist);
				TargetRotation = Quaternion.LookRotation (targetPoint - transform.position);
				transform.rotation = TargetRotation;
			}
			
			if (Network.isServer) 
			{
				Transform t = Network.Instantiate (consecration, targetPosition, this.transform.rotation, 0) as Transform;
			}
			else if (!Network.isClient)
			{
				Transform t = Instantiate (consecration, targetPosition, this.transform.rotation) as Transform;
			}
			ConsecrationCooldown=90;
		}
	}

	void DoSkillbarChanges()
	{
		if(TeleportCooldown<=0)
		{
			TeleportCooldown=0;
		}

		if(AuraCooldown<=0)
		{
			AuraCooldown=0;
		}

		if(CleaveCooldown<=0)
		{
			CleaveCooldown=0;
		}

		if(ConsecrationCooldown<=0)
		{
			ConsecrationCooldown=0;
		}

		q.text = (int)TeleportCooldown + "";
		w.text = (int)AuraCooldown + "";
		e.text = (int)CleaveCooldown + "";
		r.text = (int)ConsecrationCooldown + "";

		if(specificAttack=="q")
		{
			qAttack.texture = qAttackB;
		}else
		{
			qAttack.texture = qAttackA;
		}

		if(specificAttack=="w")
		{
			wAttack.texture = wAttackB;
		}else
		{
			wAttack.texture = wAttackA;
		}

		if(specificAttack=="e")
		{
			eAttack.texture = eAttackB;
		}else
		{
			eAttack.texture = eAttackA;
		}

		if(specificAttack=="r")
		{
			rAttack.texture = rAttackB;
		}else
		{
			rAttack.texture = rAttackA;
		}
	}
}
