﻿using UnityEngine;
using System.Collections;

public class MobCollisionManager : CollisionManager 
{

	public virtual void OnTriggerStay(Collider other)
	{
		doWallChecks(other);
		doMobChecks(other);
	}
	
	void OnTriggerEnter(Collider other) 
	{
		doWallChecks(other);
		doMobChecks(other);
		doAttackChecks (other);
	}


	//the player's projectile does 1 damage to the mob.
	public void doAttackChecks(Collider other)
	{
		if(other.tag == "projectile")
		{

			//only two cases so this is a really easy way to deal with hitting a boss instead of a mob
			try
			{
				MobBehaviour behaviour = this.gameObject.GetComponent<MobBehaviour>();
				behaviour.health-=1;
				//Debug.Log(behaviour.health);
			}
			catch
			{
				SpawnerBehaviour behaviour = this.gameObject.GetComponent<SpawnerBehaviour>();
				behaviour.health-=1;
				//Debug.Log(behaviour.health);
			}
		}

		if(other.tag == "Cleave")
		{
			try
			{
				MobBehaviour behaviour = this.gameObject.GetComponent<MobBehaviour>();
				behaviour.health-=10;
			}
			catch
			{
				SpawnerBehaviour behaviour = this.gameObject.GetComponent<SpawnerBehaviour>();
				behaviour.health-=10;
			}
		}
	}

	//I didn't want mobs to enter eachother
	//so every time they touch, they repel very slightly
	public void doMobChecks(Collider other)
	{
		if(other.tag == "Mob")
		{
			if(other.gameObject.transform.position.x > this.transform.position.x)
			{
				this.transform.position = new Vector3(this.transform.position.x-0.07f, this.transform.position.y, this.transform.position.z);
			}
			if(other.gameObject.transform.position.x < this.transform.position.x)
			{
				this.transform.position = new Vector3(this.transform.position.x+0.07f, this.transform.position.y, this.transform.position.z);
			}
			if(other.gameObject.transform.position.x > this.transform.position.y)
			{
				this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y-0.07f, this.transform.position.z);
			}
			if(other.gameObject.transform.position.x < this.transform.position.y)
			{
				this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y+0.07f, this.transform.position.z);
			}
		}
	}

	//I had to override this because mobs behave differently from the player
	public override void doWallChecks(Collider other)
	{
		if (other.tag=="TopWall")
		{
			this.transform.position=new Vector3(transform.position.x, transform.position.y, transform.position.z-0.1f);
		}else 
			if(other.tag=="BottomWall")
		{
			this.transform.position=new Vector3(transform.position.x, transform.position.y, transform.position.z+0.1f);
		}else 
			if(other.tag=="LeftWall")
		{
			this.transform.position=new Vector3(transform.position.x+0.1f, transform.position.y, transform.position.z);
		}else 
			if(other.tag=="RightWall")
		{
			this.transform.position=new Vector3(transform.position.x-0.1f, transform.position.y, transform.position.z);
		}
	}
}
