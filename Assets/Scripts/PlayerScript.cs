﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerScript : MonoBehaviour 
{
	public static PlayerScript instance;


	//this script holds the player's stats.
	//there's no real reason for them to be in a separate script
	//outside of the fact that it's better for me to have some small
	//classes instead of a large one

	public static float defaultSpeedMultiplier=1f, defaultHealth = 100f;
	public float health = defaultHealth, speedMultiplier = defaultSpeedMultiplier;
	public float effectCooldown = 3f;
	public int MobsKilled = 0;
	public GUITexture healthPrefab;
	[HideInInspector]
	public GUITexture healthBar;
	
	private GUIText text;
	private GUIText mobsText;
	private float startTime;


	void Start()
	{
		PlayerPrefs.SetFloat ("time", 0);

		if(Application.loadedLevel==2)
		{
			Debug.Log(Time.time);
			startTime = Time.time;
		}


		instance = this;

		if(Application.loadedLevel!=0)
		{
			healthBar = Instantiate (healthPrefab, new Vector3 (0.95f, 0.05f, 1), Quaternion.identity) as GUITexture;
			text = healthBar.transform.FindChild ("HealthText").GetComponent<GUIText> ();
			mobsText = healthBar.transform.FindChild ("Souls").GetComponent<GUIText> ();
		}
	}

	
	// Update is called once per frame
	void Update () 
	{
		if(Network.isClient)
		{
			healthBar.gameObject.SetActive(false);
		}

		if(Application.loadedLevel!=0)
		{
			healthBar.transform.position = new Vector3 (0.95f, 0.05f, 1);
			healthBar.guiTexture.pixelInset = new Rect(0, 0, 30, 100*(health/defaultHealth));
			
			text.text = health + "";
			mobsText.text = MobsKilled + "";
		}


		if(!Network.isClient)
		{
			if (health <= 0)
			{
				health = 0;
				Network.Disconnect();
				KeepStats();
				Application.LoadLevel(3);
			}

			if(effectCooldown <=0)
			{
				speedMultiplier = defaultSpeedMultiplier;
				effectCooldown = 3f;
			}
			else
			{
				effectCooldown-=Time.deltaTime;
			}
		}
	}


	public void KeepStats()
	{
		PlayerPrefs.SetFloat ("hp", health);
		PlayerPrefs.SetFloat ("kills", MobsKilled);
		float x;

		if(Application.loadedLevel==2)
		{
			x = Time.time - startTime;
		}else 
		{
			x=0;
		}

		PlayerPrefs.SetFloat ("time", x);
	}
}
