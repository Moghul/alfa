﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MobRandomizer : MonoBehaviour {

	//this class attaches to each tile and spawns 2 random mobs


	public Transform[] possibleMobs;
	public Transform room;
	public List<Transform> actualMobs;

	public static MobRandomizer instance;

	void Awake()
	{
		instance=this;
		actualMobs = new List<Transform>();
	}

	void Start()
	{
		StartCoroutine (RandomizeMobs ());
	}

	// Use this for initialization
	IEnumerator RandomizeMobs () 
	{
		yield return new WaitForSeconds (1.0f);

		if(!Network.isClient)
		{
			if(possibleMobs.Length!=0)
			{
				while (actualMobs.Count<2)
				{
					int temp = Random.Range(0, possibleMobs.Length);

					Transform x;
					Vector3 pos = new Vector3((this.transform.position.x+actualMobs.Count+0.3f), this.transform.position.y+5, this.transform.position.z);
					
					if(Network.isServer)
					{
						x = Network.Instantiate(possibleMobs[temp], pos, this.transform.rotation, 0) as Transform;
						NetworkParentSolver.instance.SyncMount();
						actualMobs.Add(x);
					}else if (!Network.isClient)
					{
						x = Transform.Instantiate(possibleMobs[temp], pos, this.transform.rotation) as Transform;
						x.transform.parent = room;
						actualMobs.Add(x);
					}
				}
			}

		}
	}
}
