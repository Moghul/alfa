﻿using UnityEngine;
using System.Collections;
using System.IO;

public class LevelLoader : MonoBehaviour 
{
	public static LevelLoader instance;

	void Awake ()
	{
		instance = this;
	}
	
	void OnDisconnectedFromServer ()
	{
		Application.LoadLevel(0);
	}


	/// <summary>
	/// This method takes our save file and interprets it into a level.
	/// Because the data is saved in a liniar fashion, we need to organize it properly.
	/// This is why we have two counters to handle when to move to the next row of tiles.
	/// It starts from the bottom, creates 3 tiles, moves one row up and repeats
	/// until there are 9 tiles
	/// </summary>
	public void LoadLevel()
	{
		int counterOne = 0;
		int counterTwo = 0;

		for (int j = 1; j<LevelRandomizer.instance.saveData.Count; j++)
		{
			for (int i = 0; i< LevelRandomizer.instance.possibleRooms.Length; i++)
			{
				if (LevelRandomizer.instance.saveData[j] == LevelRandomizer.instance.possibleRooms[i].name)
				{
					if(counterOne==3)
					{
						counterTwo++;
						counterOne=0;
					}
					
					Vector3 pos = new Vector3(counterTwo*20f, -3f, counterOne*20f);
					Quaternion rot = new Quaternion(0f, 0f, 0f, 0f);
					
					counterOne++;
					
					if(Network.isServer)
					{
						LevelRandomizer.instance.t=Network.Instantiate(LevelRandomizer.instance.possibleRooms[i], pos, rot, 0) as Transform;
						//t.transform.parent=level;
						NetworkParentSolver.instance.solveParenting(LevelRandomizer.instance.t, LevelRandomizer.instance.level);
						LevelRandomizer.instance.actualRooms.Add(LevelRandomizer.instance.t);
					}else if (NetworkManagerScript.isClient == false)
					{
						LevelRandomizer.instance.t=Instantiate(LevelRandomizer.instance.possibleRooms[i], pos, rot) as Transform;
						LevelRandomizer.instance.t.transform.parent=LevelRandomizer.instance.level;
						LevelRandomizer.instance.actualRooms.Add(LevelRandomizer.instance.t);
					}
				}
			}
		}
	}



	
	/// <summary>
	/// Basically, this method creates a save file with the player's current stats. For the moment, it is just the health
	/// </summary>
	public void PrepareNextLevel()
	{
		if(!Network.isClient)
		{
			if (!Directory.Exists(Application.persistentDataPath + "/SaveGame"))
			{
				Directory.CreateDirectory(Application.persistentDataPath + "/SaveGame");
				//Debug.Log ("Created save file");
			}
			
			if (LevelRandomizer.instance.saveData != null) 
			{
				LevelRandomizer.instance.saveData.Clear ();
			}
			LevelRandomizer.instance.saveData.Add (PlayerScript.instance.health+"");
			
			ParseToSaveFile (LevelRandomizer.instance.nextLevelPath);
			//Debug.Log("wrote to file");
		}
	}
	
	
	/// <summary>
	/// This method saves the state of the game at that point in time.
	/// It saves the tiles and the player's state.
	/// </summary>
	public void SaveGame()
	{
		if (!Network.isClient)
		{
			if (!Directory.Exists(Application.persistentDataPath + "/SaveGame"))
			{
				Directory.CreateDirectory(Application.persistentDataPath + "/SaveGame");
			}
			
			if(File.Exists(LevelRandomizer.instance.savePath))
			{
				File.Delete(LevelRandomizer.instance.savePath);
			}
			
			LevelRandomizer.instance.saveData.Clear ();
			LevelRandomizer.instance.saveData.Add (PlayerScript.instance.health+"");
			
			for (int i = 0; i<LevelRandomizer.instance.actualRooms.Count; i++)
			{
				string x = LevelRandomizer.instance.actualRooms[i].name;
				//x.Replace("(Clone)", "");
				x = x.Remove(x.Length-7);
				Debug.Log (x);
				LevelRandomizer.instance.saveData.Add(x);
			}
			
			ParseToSaveFile (LevelRandomizer.instance.savePath);
		}
	}
	
	//read function for files. we split the data at the '|' point because that is a separator
	public void ParseFromSaveFile(string path)
	{
		StreamReader read = new StreamReader(path);
		string data = read.ReadToEnd();
		string[] dataArray = data.Split('|');
		LevelRandomizer.instance.saveData.Clear ();
		
		for(int i = 0; i<dataArray.Length; i++)
		{
			LevelRandomizer.instance.saveData.Add(dataArray[i]);
		}
		
		
		read.Close();
	}
	
	//write function for files. we add a '|' character after each element 
	//so that later we can split them up and tell them apart easier
	public void ParseToSaveFile(string path)
	{
		string x;
		x = LevelRandomizer.instance.saveData [0];
		
		for (int i = 1; i<LevelRandomizer.instance.saveData.Count; i++)
		{
			x += "|" + LevelRandomizer.instance.saveData[i];
		}
		Debug.Log (x);
		
		if(File.Exists(path))
		{
			File.Delete(path);
		}
		StreamWriter write = new StreamWriter(path, true);
		write.Write(x);
		write.Close();
	}
}
