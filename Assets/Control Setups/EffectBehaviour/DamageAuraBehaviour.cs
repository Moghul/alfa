﻿using UnityEngine;
using System.Collections;

public class DamageAuraBehaviour : MonoBehaviour {

	//simple script for the angel's aura

	float rotSpeed;

	// Use this for initialization
	void Start () 
	{
		rotSpeed = 7f;
	}
	
	// Update is called once per frame
	void Update () 
	{
			//it spins constantly at speed 7 for some animation
			this.gameObject.transform.Rotate (0f, rotSpeed, 0f);
	}

	void OnTriggerEnter(Collider other)
	{
		//it slows down the player when they enter it
		if (other.tag == "Player") 
		{
			PlayerScript.instance.speedMultiplier = 0.7f;
		}
	}

	void OnTriggerStay (Collider other)
	{
		//it keeps the player slowed while in it
		if (other.tag == "Player") 
		{
			PlayerScript.instance.speedMultiplier = 0.7f;
		}
	}

	void OnTriggerExit (Collider other)
	{
		//it gives the player a speed boost on exiting but it also lowers their health
		if (other.tag == "Player") 
		{
			PlayerScript.instance.speedMultiplier = 2f;
			PlayerScript.instance.health-=5f;
		}
	}
}
