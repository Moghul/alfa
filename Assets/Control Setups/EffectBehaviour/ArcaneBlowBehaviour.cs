﻿using UnityEngine;
using System.Collections;

public class ArcaneBlowBehaviour : EnemyProjectileBehaviour 
{
	//this is a projectile that has a good number of effects but low speed.
	//it drops health, it freezes you and it increases all your cooldowns
	//but it moves at about half your speed.

	public override void performEffect()
	{
		PlayerScript.instance.health -= 30;
		PlayerScript.instance.speedMultiplier = 0.2f;
		PlayerScript.instance.effectCooldown = 4;
	}	
	
	public override void initialize()
	{
		playerPosition = ClickToMove.instance.gameObject.transform.position;
		playerRotation = ClickToMove.instance.gameObject.transform.rotation;
		projectileSpeed = 5;
	}
}
