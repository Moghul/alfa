﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConsecrationBehaviour : MonoBehaviour 
{
	public Vector3 targetPosition;


	private float duration;
	private List<GameObject> touchedMobs = new List<GameObject>();

	void Start()
	{
		duration = 20f;
	}

	void OnDestroy()
	{
		Debug.Log ("destroyed");
		for(int i = 0; i< touchedMobs.Count; i++)
		{
			touchedMobs[i].GetComponent<MobBehaviour>().multiplier=1;
		}
	}

	void Update()
	{
		duration -= Time.deltaTime;

		if (duration<=0)
		{
			if(Network.isServer)
			{
				Network.Destroy(this.gameObject);
			}else 
			if(!Network.isClient)
			{
				Destroy(this.gameObject);
			}
		}
	}


	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Mob") 
		{
			MobBehaviour behaviour = other.gameObject.GetComponent<MobBehaviour>();
			behaviour.multiplier = 0;
			touchedMobs.Add (other.gameObject);
		}
	}

}
