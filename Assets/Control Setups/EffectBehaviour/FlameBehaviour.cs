﻿using UnityEngine;
using System.Collections;

public class FlameBehaviour : EnemyProjectileBehaviour {

	public override void performEffect()
	{
		PlayerScript.instance.health -= 5;
	}	

	//basic projectile that moves at a decent speed and does a low amount of damage

	public override void initialize()
	{
		playerPosition = ClickToMove.instance.gameObject.transform.position;
		playerRotation = ClickToMove.instance.gameObject.transform.rotation;
		projectileSpeed = 6;
	}
}
