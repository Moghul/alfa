﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour {


	//this script handles the projectile that the player spawns
	public Vector3 targetPosition, startingPosition;
	public Quaternion targetRotation;
	public float projectileSpeed;

	// Use this for initialization
	void Start () 
	{
		projectileSpeed = 9f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!Network.isClient)
		{
			this.transform.position = Vector3.MoveTowards (this.transform.position, targetPosition, Time.deltaTime * projectileSpeed);
			transform.rotation = targetRotation;

			//I don't want it to fly forever and i don't want it to hang around after it missed.
			if(this.transform.position == targetPosition || Vector3.Distance(this.transform.position, startingPosition)>9f)
			{
				if (Network.isServer)
				{
					Network.Destroy(this.gameObject);
				}else if(!Network.isClient)
				{
					Destroy(this.gameObject);
				}
			}
		}
	}

	//it's meant to ignore the player and his aura
	//once it hits anything, it is destroyed.
	//the target handles the collision with the projectile
	public void OnTriggerEnter(Collider other)
	{
		if (other.tag!="Player" && other.tag!="aura")
		{
			if (Network.isServer)
			{
				Network.Destroy(this.gameObject);
			}else if(!Network.isClient)
			{
				Destroy(this.gameObject);
			}
		}
	}

	public void OnTriggerStay(Collider other)
	{
		if (other.tag!="Player" && other.tag!="aura")
		{
			if (Network.isServer)
			{
				Network.Destroy(this.gameObject);
			}else if(!Network.isClient)
			{
				Destroy(this.gameObject);
			}
		}
	}

}
