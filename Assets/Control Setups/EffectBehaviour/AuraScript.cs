﻿using UnityEngine;
using System.Collections;

public class AuraScript : MonoBehaviour {
	
	float effectInterval = 2f;

	// Use this for initialization
	void Start () {
	
	}

	//this particular aura script is the player's aura.
	//it's a healing spell and that's about all there is to it.

	// Update is called once per frame
	void Update () 
	{
		if(!Network.isClient)
		{
			if(effectInterval <=0)
			{
				effectInterval = 2f;
				if(PlayerScript.instance.health < PlayerScript.defaultHealth)
				{
					PlayerScript.instance.health +=5;
				}
			}else
			{
				effectInterval-=Time.deltaTime;
			}
		}
	}
}
