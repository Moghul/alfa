﻿using UnityEngine;
using System.Collections;

public abstract class EnemyProjectileBehaviour : MonoBehaviour {

	public Vector3 playerPosition;
	public Quaternion playerRotation;
	public float projectileSpeed;

	// Use this for initialization
	void Start () 
	{
		initialize ();
	}

	//this class is made to be the model for the various projectiles the enemies spawn.
	//once it spawns, it gets a target position and starts working its way there
	//if it doesn't hit anything, it is destroyed to avoid flooding the game with gameobjects
	//and crashing it

	// Update is called once per frame
	public virtual void Update () 
	{
		if(!Network.isClient)
		{
			this.transform.position = Vector3.MoveTowards (this.transform.position, playerPosition, Time.deltaTime * projectileSpeed);
			if (this.transform.position == playerPosition) 
			{
				if (Network.isServer)
				{
					Network.Destroy(this.gameObject);
				}else if(NetworkManagerScript.isClient == false)
				{
					Destroy(this.gameObject);
				}
			}
		}
	}

	//when it touches the player, the projectile does something and then it is destroyed.
	public void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			performEffect ();
			if (Network.isServer)
			{
				Network.Destroy(this.gameObject);
			}else if(NetworkManagerScript.isClient == false)
			{
				Destroy(this.gameObject);
			}
		}
	}

	public virtual void performEffect()
	{

	}	

	//this method sets the target, rotation and speed of the projectile.

	public virtual void initialize()
	{
		playerPosition = ClickToMove.instance.gameObject.transform.position;
		playerRotation = ClickToMove.instance.gameObject.transform.rotation;
		projectileSpeed = 8;
	}
}
