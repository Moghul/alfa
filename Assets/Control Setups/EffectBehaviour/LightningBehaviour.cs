﻿using UnityEngine;
using System.Collections;

public class LightningBehaviour : EnemyProjectileBehaviour {

	public override void performEffect()
	{
		PlayerScript.instance.health -= 15;
	}	

	//this projectile moves fast and does quite a good amount of damage

	public virtual void initialize()
	{
		playerPosition = ClickToMove.instance.gameObject.transform.position;
		playerRotation = ClickToMove.instance.gameObject.transform.rotation;
		projectileSpeed = 10;
	}
}
