﻿using UnityEngine;
using System.Collections;

public class RockHurlBehaviour : EnemyProjectileBehaviour {

	// Use this for initialization
	public override void performEffect()
	{
		PlayerScript.instance.health -= 20;
	}	

	//highly damaging projectile that moves slowly

	public override void initialize()
	{
		playerPosition = ClickToMove.instance.gameObject.transform.position;
		playerRotation = ClickToMove.instance.gameObject.transform.rotation;
		projectileSpeed = 4;
	}
}
