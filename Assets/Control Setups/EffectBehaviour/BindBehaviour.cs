﻿using UnityEngine;
using System.Collections;

public class BindBehaviour : EnemyProjectileBehaviour {

	//it's a simple projectile that moves very slowly and binds you in place.
	//the twist is, it follows you around and it only disappears after 5 seconds.


	private float timeUntilDeath;


	public override void performEffect()
	{
		PlayerScript.instance.speedMultiplier = 0;
	}	
	
	public override void initialize()
	{
		timeUntilDeath = 5;
		projectileSpeed = 3;
	}

	void Update()
	{
		if(!Network.isClient)
		{
			playerPosition = ClickToMove.instance.gameObject.transform.position;
			playerRotation = ClickToMove.instance.gameObject.transform.rotation;
			this.transform.position = Vector3.MoveTowards (this.transform.position, playerPosition, Time.deltaTime * projectileSpeed);

			if(timeUntilDeath<=0)
			{
				if (Network.isServer)
				{
					Network.Destroy(this.gameObject);
				}else if(NetworkManagerScript.isClient == false)
				{ 
					Destroy(this.gameObject);
				}
			}else
			{
				timeUntilDeath-=Time.deltaTime;
			}
		}
	}
}
