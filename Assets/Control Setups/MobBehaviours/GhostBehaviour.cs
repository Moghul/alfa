﻿using UnityEngine;
using System.Collections;

public class GhostBehaviour : RangedMobBehaviour
{
	public override void Initialize()
	{
		INTERVAL = 1f;
		minDistance = 7f;
	}
}
