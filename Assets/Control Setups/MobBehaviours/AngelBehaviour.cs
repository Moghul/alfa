﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AngelBehaviour : MobBehaviour 
{
	public Transform dmgAura;
	private bool hasAura;

	public override void Initialize()
	{
		minDistance = 1f;
	}

	//this monster is a little bit different. since all it ever has to do is turn on the aura,
	//it should do it when the player is close enough and it should only do it once.

	public override void Update () 
	{
		dmgAura.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y-0.1f ,this.transform.position.z);

		if(health == 0)
		{
			this.gameObject.SetActive(false);
		}
		
		if (canDoBehaviour)
		{
			MoveTowardsPlayer();
			PerformSpecificAttack();
		}
	}

	[RPC]
	public void ToggleAura(bool x) 
	{
		dmgAura.gameObject.SetActive (x);
	}
	
	public override void PerformSpecificAttack()
	{
		if (!hasAura)
		{
			if(Network.isServer)
			{
				dmgAura.gameObject.SetActive(true);
				networkView.RPC("ToggleAura", RPCMode.OthersBuffered, true);
			}else if(!Network.isClient)
			{
				dmgAura.gameObject.SetActive(true);
			}

			hasAura=true;
		}
	}
}
