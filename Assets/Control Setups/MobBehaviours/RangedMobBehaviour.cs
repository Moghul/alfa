﻿using UnityEngine;
using System.Collections;

public abstract class RangedMobBehaviour : MobBehaviour 
{
	public Transform projectile;
	
	public override bool MeetsProximityRequirements()
	{
		float distance = Vector3.Distance (ClickToMove.instance.gameObject.transform.position, this.transform.position);
		
		if(distance <= minDistance)
		{
			playerPosition=this.transform.position;
			return true;
		}
		else
		{
			return false;
		}
	}

	public override void PerformSpecificAttack()
	{
		if(PlayerScript.instance.health>0)
		{
			Transform t;
			if(Network.isServer)
			{
				t = Network.Instantiate(projectile, this.transform.position, this.transform.rotation, 0) as Transform;
			}else if (!Network.isClient)
			{
				t = Instantiate(projectile, this.transform.position, this.transform.rotation) as Transform;
			}
		}
	}
}
