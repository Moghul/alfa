using UnityEngine;
using System.Collections;

public abstract class MobBehaviour : MonoBehaviour {

	public Vector3 playerPosition;
	public float minDistance, health, multiplier, deestance=0;
	public bool canDoBehaviour;
	public static float INTERVAL;
	private float attackInterval = INTERVAL;

	// Use this for initialization
	void Start () 
	{
		multiplier = 1;
		health = 10;
		Initialize();
	}

	[RPC]
	public void ToggleThis() 
	{
		this.gameObject.SetActive (false);
	}

	//this is the parent class to all the non-boss mobs.
	//it checks distance and when you enter the room, canDoBehaviour becomes true. (this is done in PlayerPosChecker.cs)
	//once you're in the room, the mob moves towards you and once you're in range, it attacks.

	// Update is called once per frame
	public virtual void Update () 
	{
		if(health <= 0)
		{
			PlayerScript.instance.MobsKilled+=1;

			if(Network.isServer)
			{
				networkView.RPC("ToggleThis", RPCMode.Server);
				ToggleThis();
			}else
			{
				ToggleThis();
			}
		}

		if(!Network.isClient)
		{
			this.transform.position = new Vector3(this.transform.position.x, -2f, this.transform.position.z);

			if (canDoBehaviour)
			{
				if(MeetsProximityRequirements())
				{
					Attack();
				}else
				{
					MoveTowardsPlayer();
				}
			}
		}
	}

	public virtual void Initialize()
	{
		minDistance=1f;
		INTERVAL = 0.2f;
	}

	public virtual bool MeetsProximityRequirements()
	{
		return false;
	}

	public virtual void PerformSpecificAttack()
	{

	}

	private void Attack()
	{
		if(attackInterval<=0)
		{
			attackInterval= INTERVAL;
			PerformSpecificAttack();
		}
		else
		{
			attackInterval-=Time.deltaTime;
		}
	}

	public void MoveTowardsPlayer()
	{
		playerPosition = ClickToMove.instance.gameObject.transform.position;
		transform.position = Vector3.MoveTowards (transform.position, playerPosition, Time.deltaTime * 2.5f * multiplier);
	}

}
