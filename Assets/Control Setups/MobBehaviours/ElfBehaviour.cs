﻿using UnityEngine;
using System.Collections;

public class ElfBehaviour : RangedMobBehaviour 
{
	public Transform[] projectiles;

	public override void Initialize()
	{
		INTERVAL = 2f;
		minDistance = 7f;
	}
	
	public override void PerformSpecificAttack()
	{
		if(PlayerScript.instance.health>0)
		{
			int x = Random.Range(0, projectiles.Length-1);
			Transform t;
			if(Network.isServer)
			{
				t = Network.Instantiate(projectiles[x], this.transform.position, this.transform.rotation, 0) as Transform;
			} else if (!Network.isClient)
			{
				t = Instantiate(projectiles[x], this.transform.position, this.transform.rotation) as Transform;
			}
		}
	}
}
