﻿using UnityEngine;
using System.Collections;

public class DruidBehaviour : MobBehaviour 
{
	public Sprite[] sprites;
	SpriteRenderer rend;

	public override void Initialize()
	{
		minDistance = 1f;
		rend = this.transform.FindChild("Sprite").GetComponent("SpriteRenderer") as SpriteRenderer;
	}

	//the druid transforms into a bear. i did this by creating a sprite array
	//and switching between elements in the array

	public override bool MeetsProximityRequirements()
	{
		float distance = Vector3.Distance (ClickToMove.instance.gameObject.transform.position, this.transform.position);

		if(distance <= 3)
		{
			rend.sprite = sprites[1];
		}
		else
		{
			rend.sprite = sprites[0];
		}

		if(distance <= minDistance)
		{
			playerPosition=this.transform.position;
			return true;
		}else
			return false;
	}
	
	public override void PerformSpecificAttack()
	{
		if(PlayerScript.instance.health>0)
		{
			PlayerScript.instance.health-=10;
		}
	}
}
