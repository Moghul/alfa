﻿using UnityEngine;
using System.Collections;

public class YetiBehaviour : MobBehaviour {

	public override void Initialize()
	{
		minDistance = 1f;
	}
	
	public override bool MeetsProximityRequirements()
	{
		float distance = Vector3.Distance (ClickToMove.instance.gameObject.transform.position, this.transform.position);

		if(distance <= 3)
		{
			PlayerScript.instance.speedMultiplier = 0.8f * PlayerScript.defaultSpeedMultiplier;
		}
		else
		{
			PlayerScript.instance.speedMultiplier = PlayerScript.defaultSpeedMultiplier;
		}

		if(distance <= minDistance)
		{
			playerPosition=this.transform.position;
			return true;
		}else
			return false;
	}
	
	public override void PerformSpecificAttack()
	{
		if(PlayerScript.instance.health>0)
		{
			PlayerScript.instance.health-=5;
		}
	}
}
