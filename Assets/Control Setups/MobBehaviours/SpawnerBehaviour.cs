﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnerBehaviour : MonoBehaviour {

	public Transform[] possibleMobs;
	public List<Transform> realMobs;
	private float interval = 5f;
	public float health = 50;

	public static SpawnerBehaviour instance;

	// Use this for initialization
	void Start () 
	{ 
		instance = this;
	}

	//The spawner is a boss monster and it spawns other mobs randomly.
	//The reason it's not inheriting anything from MobBehaviour is because this
	//monster doesn't move. It just sits there and lets the others do things for him

	// Update is called once per frame
	void Update () 
	{			
		this.gameObject.transform.localPosition = new Vector3 (4.6f, 0.1f, 0);

		if (interval <= 0) 
		{
			int x = Random.Range(0, possibleMobs.Length-1);
			Vector3 pos = new Vector3(this.transform.position.x-5f, this.transform.position.y, this.transform.position.z); 
			Transform t;

			if(Network.isServer)
			{
				t = Network.Instantiate(possibleMobs[x], pos, this.transform.rotation, 0) as Transform;
				NetworkParentSolver.instance.SyncMount();
				realMobs.Add(t);
			}else if (!Network.isClient)
			{
				t = Transform.Instantiate(possibleMobs[x], pos, this.transform.rotation) as Transform;
				t.transform.parent = this.transform;
				realMobs.Add(t);
			}
			interval = 5f;
		} else 
		{
			interval-=Time.deltaTime;
		}

		if (health <= 0) 
		{
			Network.Disconnect();
			PlayerScript.instance.KeepStats();
			Application.LoadLevel(3);
		}
	}
}
