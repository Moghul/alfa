﻿using UnityEngine;
using System.Collections;

public class GargoyleBehaviour : MobBehaviour 
{
	
	public override void Initialize()
	{
		minDistance = 1f;
	}

	//the gargoyle is the simplest of the mobs, all it does is get close and hit you

	public override bool MeetsProximityRequirements()
	{
		float distance = Vector3.Distance (ClickToMove.instance.gameObject.transform.position, this.transform.position);

		if(distance <= minDistance)
		{
			playerPosition=this.transform.position;
			return true;
		}else
			return false;
	}
	
	public override void PerformSpecificAttack()
	{
		if(PlayerScript.instance.health>0)
		{
			PlayerScript.instance.health-=10;
		}
	}

}
