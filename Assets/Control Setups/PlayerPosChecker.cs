using UnityEngine;
using System.Collections;

public class PlayerPosChecker : MonoBehaviour {

	public static PlayerPosChecker instance;
	private float distance;

	MobBehaviour behaviour;
	MobRandomizer randomizer;

	// Use this for initialization
	void Start () 
	{ 
		instance = this;
	}

	//this is where we tell the mobs they can move towards us and subsequently attack us
	//i used a try/catch statement because there are only two possible outcomes
	//you are either in the regular levels or you are fighting the boss.

	// Update is called once per frame
	void Update () 
	{
		if(!Network.isClient)
		{
			try
			{
				for (int i = 0; i<LevelRandomizer.instance.actualRooms.Count; i++)
				{

					//the try will most likely catch an exception here.
					//I'm checking every room one by one and I'm looking how far I am from it
					distance = Vector3.Distance(this.transform.position, LevelRandomizer.instance.actualRooms[i].transform.localPosition);
					randomizer = LevelRandomizer.instance.actualRooms[i].GetComponent("MobRandomizer") as MobRandomizer;

					for (int j = 0; j< randomizer.actualMobs.Count; j++)
					{
						behaviour = randomizer.actualMobs[j].GetComponent("MobBehaviour") as MobBehaviour;
						//if i'm closer than 10 (half a room's width) to the center of the room
						//i tell all the mobs in it that they can now start their behaviours
						if(distance <=10.5f)
						{
							behaviour.canDoBehaviour=true;
							behaviour.deestance = distance;
						}
						else
						{
							behaviour.canDoBehaviour=false;
							behaviour.deestance = distance;
						}
					}
				}
			}
			catch
			{
				try{
					//if i'm in a boss fight, all I care about is how far the boss is from me
					//and as long as i'm on the boss tile, I make the mobs attack me.
					for(int i = 0; i<SpawnerBehaviour.instance.realMobs.Count; i++)
					{
						try
						{
							distance = Vector3.Distance(this.transform.position, SpawnerBehaviour.instance.realMobs[i].transform.localPosition);
						}catch 
						{
							Debug.Log("error");
						}

						behaviour = SpawnerBehaviour.instance.realMobs[i].GetComponent("MobBehaviour") as MobBehaviour;

						if(distance <=100)
						{
							behaviour.canDoBehaviour=true;
						}
						else
						{
							behaviour.canDoBehaviour=false;
						}
					}
				}catch
				{

				}
			}
		}
	}
}
