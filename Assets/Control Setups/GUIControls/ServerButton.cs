﻿using UnityEngine;
using System.Collections;

public class ServerButton : MonoBehaviour 
{
	public HostData host;

	void OnMouseDown()
	{
		Network.Connect(host);
		Application.LoadLevel(1);
	}
}
