﻿using UnityEngine;
using System.Collections;

public class EndScreenGui : MonoBehaviour {

	public GUIText[] texts;

	private float x = 0;
	// Use this for initialization
	void Start () 
	{
		texts [0].text = "You were left with " + PlayerPrefs.GetFloat ("hp") + " HP";


		x = Mathf.Round (PlayerPrefs.GetFloat ("time"));
		if(x>0)
		{
			texts [1].text = "Boss fight lasted for " + x + " s";
		}else
		{
			texts [1].text = "You never got to the boss ";
		}
		texts [2].text = "You killed " + PlayerPrefs.GetFloat("kills") + " mobs";
	}
}
