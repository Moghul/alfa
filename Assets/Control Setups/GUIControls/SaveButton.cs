﻿using UnityEngine;
using System.Collections;

public class SaveButton : MonoBehaviour 
{
	public GUITexture label;

	void OnMouseDown()
	{
		LevelLoader.instance.SaveGame ();
		label.gameObject.SetActive(true);
	}
}
