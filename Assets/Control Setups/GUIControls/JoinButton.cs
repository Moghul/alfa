﻿using UnityEngine;
using System.Collections;

public class JoinButton : MonoBehaviour 
{
	public Transform serverBtn;

	[HideInInspector]
	public Transform t;
	
	void OnMouseDown()
	{
		NetworkManagerScript.instance.refreshHostList();

		if (NetworkManagerScript.instance.hostData!=null) 
		{
			for (int i = 0; i<NetworkManagerScript.instance.hostData.Length; i++) 
			{
				if(t==null)
				{
					t = Instantiate(serverBtn, new Vector3(0.77f, 0.8f - (i*0.15f) , 0), Quaternion.identity) as Transform;
					t.parent = this.transform.parent;
					GUIText x = t.transform.FindChild("GUITEXT").GetComponent<GUIText>();
					x.text = NetworkManagerScript.instance.hostData[i].gameName.ToUpper();

					ServerButton s = t.GetComponent<ServerButton>();
					s.host = NetworkManagerScript.instance.hostData[i];
				}
			}
		}else 
		{
			StartCoroutine("TryAgain");
		}
	}


	IEnumerator TryAgain()
	{
		yield return new WaitForSeconds (0.5f);
		OnMouseDown ();
	}
}
