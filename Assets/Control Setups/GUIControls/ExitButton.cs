﻿using UnityEngine;
using System.Collections;

public class ExitButton : MonoBehaviour 
{
	void OnMouseDown()
	{

		if(Application.loadedLevel == 0)
		{
			Application.Quit ();
		}else 
		{
			ClickToMove.targetPosition = new Vector3(0, 0, 0);
			Application.LoadLevel(0);
		}
	}
}
