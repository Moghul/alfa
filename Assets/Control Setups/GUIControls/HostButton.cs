﻿using UnityEngine;
using System.Collections;

public class HostButton : MonoBehaviour 
{
	public Transform textField;
	public static Transform t;

	void OnMouseDown()
	{
		if (t==null)
		{
			Vector3 v = new Vector3(0.086f, 0.98f, this.transform.position.z);

			t = Instantiate(textField, v, Quaternion.identity) as Transform;
		}
	}
}
