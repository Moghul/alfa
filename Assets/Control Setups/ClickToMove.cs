﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClickToMove : MonoBehaviour
{
	public static ClickToMove instance;

	public Transform bullsEye;
	private Transform t;
	//used to speed up or slow down the player depending on what he is hit with.
	public float buffMultiplier;
	public static Vector3 targetPosition, targetPoint;
	private Quaternion targetRotation;
	public Camera cam;
	float hitdist;
	Plane playerPlane;
	Ray ray;

	void Awake()
	{
		//targetPosition = this.gameObject.transform.position;
		instance = this;
	}

	// Use this for initialization
	void Start ()
	{
		buffMultiplier = 1f;
	}

	// Update is called once per frame
	void Update ()
	{

		this.gameObject.transform.position = new Vector3 (this.gameObject.transform.position.x, -2f, this.gameObject.transform.position.z);


		if (targetPosition != null && !Network.isClient) 
		{
			buffMultiplier = PlayerScript.instance.speedMultiplier;

			ManageBorders();


			//when we right click
			if (Input.GetKeyDown (KeyCode.Mouse1)) 
			{
				PlayerAttack.specificAttack="d";
				hitdist = 0.0f;
				
				ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				playerPlane = new Plane (Vector3.up, transform.position);

				//we get the position of where we right clicked
				if (playerPlane.Raycast (ray, out hitdist)) 
				{
					targetPoint = ray.GetPoint (hitdist);
					targetPosition = ray.GetPoint (hitdist);
					//we turn to "look" at it
					targetRotation = Quaternion.LookRotation (targetPoint - transform.position);
					transform.rotation = targetRotation;
				}
				//we deal with the bullseye
				DestinationMarkerMovement();

			}

			//we make the camera follow us
			cam.transform.position = new Vector3 (this.transform.position.x, 22f, this.transform.position.z-9f);
			//we move from our position, to the target position at a certain speed
			transform.position = Vector3.MoveTowards (transform.position, targetPosition, Time.deltaTime * buffMultiplier * 6);
		}
	}
	/// <summary>
	/// The rooms have holes in their sides, including the edges. I wanted to make the player feel like
	/// there was no escape and I wanted to give the game some strategic teleports. For example, I can
	/// use the corners to swiftly get back to the starting tile and heal myself.
	/// </summary>
	void ManageBorders()
	{
		if(this.transform.position.x>50)
		{
			Vector3 x = new Vector3(-10, this.transform.position.y, this.transform.position.z);
			this.transform.position=x;
		}else 
			if(this.transform.position.x<-10)
		{
			Vector3 x = new Vector3(50, this.transform.position.y, this.transform.position.z);
			this.transform.position=x;
		}else
			if(this.transform.position.z>50)
		{
			Vector3 x = new Vector3(this.transform.position.x, this.transform.position.y, -10);
			this.transform.position=x;
		}else
			if(this.transform.position.z<-10)
		{
			Vector3 x = new Vector3(this.transform.position.x, this.transform.position.y, 50);
			this.transform.position=x;
		}
	}


	/// <summary>
	/// This method handles the bullseye you see when you right click a position.
	/// </summary>
	void DestinationMarkerMovement () 
	{
		if (Input.GetKeyDown (KeyCode.Mouse1)) 
		{
			if(t==null)
			{
				if (Network.isServer)
				{
					t = Network.Instantiate(bullsEye, targetPosition, targetRotation, 0) as Transform;
				}
				else
				{
					t = Instantiate (bullsEye, targetPosition, targetRotation) as Transform;
				}
			}
			t.position = new Vector3(targetPosition.x, targetPosition.y, targetPosition.z);
		}
	}
}
